/* vim:set et sw=2 cin cino=t0,f0,(0,{s,>2s,n-s,^-s,e2s: */
/*
 * Copyright © 2016 Collabora Ltd.
 *
 * SPDX-License-Identifier: MPL-2.0
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "config.h"

#include "helloworld-app.h"

#include <mildenhall/mildenhall.h>

struct _HlwApp
{
  /*< private >*/
  GApplication parent;

  ClutterActor *stage;                  /* owned */
};

G_DEFINE_TYPE (HlwApp, hlw_app, G_TYPE_APPLICATION);

static gboolean
on_stage_destroy (ClutterActor *stage,
                  gpointer user_data)
{
  GApplication *app = user_data;

  g_debug ("The main stage is destroyed.");

  /* chain up */
  CLUTTER_ACTOR_GET_CLASS (stage)->destroy (stage);

  /* The main window is detroyed (closed) so the application
   * needs to be released */
  g_application_release (app);

  return TRUE;
}

static void
startup (GApplication *app)
{
  g_autoptr (GObject) widget_object = NULL;
  g_autoptr (ThornburyItemFactory) item_factory = NULL;
  g_autofree gchar *widget_properties_file = NULL;

  HlwApp *self = HLW_APP (app);

  g_application_hold (app);

  /* chain up */
  G_APPLICATION_CLASS (hlw_app_parent_class)->startup (app);

  /* build ui components */
  self->stage = mildenhall_stage_new (g_application_get_application_id (app));

  g_signal_connect (G_OBJECT (self->stage),
      "destroy", G_CALLBACK (on_stage_destroy),
      self);

  widget_properties_file = g_build_filename (PKGDATADIR,
                                             "text_box_prop.json",
                                             NULL);

  g_debug ("Loading wigets from the property file (%s)", widget_properties_file);

  item_factory = thornbury_item_factory_generate_widget_with_props (
        MILDENHALL_TYPE_TEXT_BOX_ENTRY,
        widget_properties_file);

  g_object_get (item_factory,
                "object", &widget_object,
                NULL);

  clutter_actor_add_child (self->stage, CLUTTER_ACTOR (widget_object));

  g_object_set (widget_object,
               "focus-cursor", TRUE,
               "text", "Hello World!",
               NULL);
}

static void
activate (GApplication *app)
{
  HlwApp *self = HLW_APP (app);

  clutter_actor_show (self->stage);
}

static void
hlw_app_dispose (GObject *object)
{
  HlwApp *self = HLW_APP (object);

  g_clear_object (&self->stage);

  G_OBJECT_CLASS (hlw_app_parent_class)->dispose (object);
}

static void
hlw_app_class_init (HlwAppClass *klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  object_class->dispose = hlw_app_dispose;

  app_class->startup = startup;
  app_class->activate = activate;
}

static void
hlw_app_init (HlwApp *self)
{
}
