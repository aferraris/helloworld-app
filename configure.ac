AC_PREREQ(2.65)

AC_INIT([helloworld-app], [0.1.0],
        [mailto:maintainers@lists.apertis.org],[helloworld-app],
        [https://git.apertis.org/git/sample-applications/helloworld-app.git/])

AX_CHECK_ENABLE_DEBUG
AC_CONFIG_MACRO_DIR([m4])
AC_CONFIG_AUX_DIR([build-aux])
AC_CONFIG_HEADERS([config.h])
AC_USE_SYSTEM_EXTENSIONS
AM_SILENT_RULES([yes])
AC_REQUIRE_AUX_FILE([tap-driver.sh])

AM_INIT_AUTOMAKE([1.11 dist-xz foreign no-dist-gzip subdir-objects tar-ustar])

AC_PROG_CXX
AM_PROG_CC_C_O
LT_INIT
PKG_PROG_PKG_CONFIG
AC_PROG_LN_S

# Each Apertis application bundle has a bundle ID.
# Refer to https://appdev.apertis.org/documentation/bundle-spec.html#bundle-id
AC_SUBST([BUNDLE_ID], [org.apertis.HelloWorldApp])
AC_DEFINE_UNQUOTED([BUNDLE_ID],"$BUNDLE_ID",[The application bundle ID])

# Dependencies
PKG_CHECK_MODULES([GLIB],
                  [glib-2.0 >= 2.42.0 gio-2.0 gobject-2.0])
PKG_CHECK_MODULES([CLUTTER],
                  [clutter-1.0])
PKG_CHECK_MODULES([MILDENHALL],
                  [mildenhall-0])
PKG_CHECK_MODULES([THORNBURY],
                  [libthornbury-0])

# Code coverage
AX_CODE_COVERAGE

# General macros
AX_COMPILER_FLAGS
AX_VALGRIND_CHECK

GOBJECT_INTROSPECTION_CHECK([0.9.7])

AC_PATH_PROG([GDBUS_CODEGEN], [gdbus-codegen])
AC_PATH_PROG([GLIB_MKENUMS], [glib-mkenums])
AC_PATH_PROG([GLIB_COMPILE_RESOURCES],[glib-compile-resources])

AC_SUBST([AM_CPPFLAGS])
AC_SUBST([AM_CFLAGS])
AC_SUBST([AM_CXXFLAGS])
AC_SUBST([AM_LDFLAGS])

# Documentation
HOTDOC_CHECK([0.8], [c, gi, devhelp])

# installed-tests
AC_ARG_ENABLE([always_build_tests],
              AS_HELP_STRING([--enable-always-build-tests],
                             [Enable always building tests (default: yes)]),,
              [enable_always_build_tests=yes])
AC_ARG_ENABLE([installed_tests],
              AS_HELP_STRING([--enable-installed-tests],
                             [Install test programs (default: no)]),,
              [enable_installed_tests=no])

AM_CONDITIONAL([ENABLE_ALWAYS_BUILD_TESTS],
               [test "$enable_always_build_tests" = "yes"])
AC_SUBST([ENABLE_ALWAYS_BUILD_TESTS],[$enable_always_build_tests])

AM_CONDITIONAL([ENABLE_INSTALLED_TESTS],
               [test "$enable_installed_tests" = "yes"])
AC_SUBST([ENABLE_INSTALLED_TESTS],[$enable_installed_tests])

AC_CONFIG_FILES([
  Makefile
])
AC_OUTPUT
